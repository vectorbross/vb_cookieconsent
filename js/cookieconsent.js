window.addEventListener("load", function(){
	var cookieOptions;

	if(window.innerWidth < 960) {
		cookieOptions = {
			"palette": {
				"popup": {
					"background": drupalSettings.vb_cookieconsent.popup_background,
					"text": drupalSettings.vb_cookieconsent.popup_text
				},
				"button": {
					"background": drupalSettings.vb_cookieconsent.button_background,
					"text": drupalSettings.vb_cookieconsent.button_color
				}
			},
			"theme": "edgeless",
			"position": "top",
			"content": {
				"message": Drupal.t('We use cookies to make sure you have the best experience on our website', {}, {"langcode": "en"}),
				"dismiss": Drupal.t('OK, understood', {}, {"langcode": "en"}),
				"link": Drupal.t('More info', {}, {"langcode": "en"}),
				"href": drupalSettings.vb_cookieconsent.href
			}
		};
	} else {
		cookieOptions = {
			"palette": {
				"popup": {
					"background": drupalSettings.vb_cookieconsent.popup_background,
					"text": drupalSettings.vb_cookieconsent.popup_text
				},
				"button": {
					"background": drupalSettings.vb_cookieconsent.button_background,
					"text": drupalSettings.vb_cookieconsent.button_color
				}
			},
			"theme": "edgeless",
			"position": "bottom-left",
			"content": {
				"message": Drupal.t('We use cookies to make sure you have the best experience on our website', {}, {"langcode": "en"}),
				"dismiss": Drupal.t('OK, understood', {}, {"langcode": "en"}),
				"link": Drupal.t('More info', {}, {"langcode": "en"}),
				"href": drupalSettings.vb_cookieconsent.href
			}
		};
	}
	if(typeof window.cookieconsent !== "undefined") {
		window.cookieconsent.initialise(cookieOptions);
	}
});