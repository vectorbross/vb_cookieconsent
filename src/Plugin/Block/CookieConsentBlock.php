<?php

namespace Drupal\vb_cookieconsent\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a 'CookieConsentBlock' block.
 *
 * @Block(
 *  id = "cookieconsent_block",
 *  admin_label = @Translation("Cookie consent block"),
 * )
 */
class CookieConsentBlock extends BlockBase implements BlockPluginInterface {


  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['#attached']['library'][] = 'vb_cookieconsent/cookieconsent';
    $build['#attached']['drupalSettings']['vb_cookieconsent']['href'] = Url::fromRoute('entity.node.canonical', ['node' => $this->configuration['nid']])->toString();
    $build['#attached']['drupalSettings']['vb_cookieconsent']['popup_background'] = $this->configuration['popup_background'];
    $build['#attached']['drupalSettings']['vb_cookieconsent']['popup_color'] = $this->configuration['popup_color'];
    $build['#attached']['drupalSettings']['vb_cookieconsent']['button_background'] = $this->configuration['button_background'];
    $build['#attached']['drupalSettings']['vb_cookieconsent']['button_color'] = $this->configuration['button_color'];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['nid'] = [
      '#type' => 'number',
      '#title' => $this->t('Cookies page ID'),
      '#default_value' => isset($config['nid']) ? $config['nid'] : ''
    ];
    $form['popup_background'] = [
      '#type' => 'color',
      '#title' => $this->t('Popup background color'),
      '#default_value' => isset($config['popup_background']) ? $config['popup_background'] : ''
    ];
    $form['popup_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Popup text color'),
      '#default_value' => isset($config['popup_color']) ? $config['popup_color'] : ''
    ];
    $form['button_background'] = [
      '#type' => 'color',
      '#title' => $this->t('Button background color'),
      '#default_value' => isset($config['button_background']) ? $config['button_background'] : ''
    ];
    $form['button_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Button color'),
      '#default_value' => isset($config['button_color']) ? $config['button_color'] : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['nid'] = $values['nid'];
    $this->configuration['popup_background'] = $values['popup_background'];
    $this->configuration['popup_color'] = $values['popup_color'];
    $this->configuration['button_background'] = $values['button_background'];
    $this->configuration['button_color'] = $values['button_color'];
  }

}
